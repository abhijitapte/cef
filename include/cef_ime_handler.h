// Copyright (c) 2014 Marshall A. Greenblatt. All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//    * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//    * Neither the name of Google Inc. nor the name Chromium Embedded
// Framework nor the names of its contributors may be used to endorse
// or promote products derived from this software without specific prior
// written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// ---------------------------------------------------------------------------
//
// The contents of this file must follow a specific format in order to
// support the CEF translator tool. See the translator.README.txt file in the
// tools directory for more information.
//

#ifndef CEF_INCLUDE_CEF_IME_HANDLER_H_
#define CEF_INCLUDE_CEF_IME_HANDLER_H_
#pragma once

#include "include/cef_base.h"
#include "include/cef_browser.h"

///
// Callback interface for asynchronous continuation of IME processing.
///
/*--cef(source=library)--*/
class CefImeCallback : public virtual CefBaseRefCounted {
 public:
  ///
  // Commit the specified |settings|.
  ///
  /*--cef()--*/
  virtual void Commit(const CefString& text) = 0;

  ///
  // Predit changed.
  ///
  /*--cef()--*/
  virtual void PreeditChanged(const CefString& text) = 0;

  ///
  // Preedit end.
  ///
  /*--cef()--*/
  virtual void PreeditEnd() = 0;

  ///
  // Preedit start.
  ///
  /*--cef()--*/
  virtual void PreeditStart() = 0;

};

///
// Implement this interface to handle IME on Linux. The methods of this class will be
// called on the browser process UI thread.
///
/*--cef(source=client)--*/
class CefImeHandler : public virtual CefBaseRefCounted {
 public:

  ///
  // Called when InputMethodContext is created
  ///
  /*--cef()--*/
  virtual void OnContextCreated(bool is_simple, CefRefPtr<CefImeCallback> callback) = 0;

  ///
  // Called when a key event is being dispatched.
  ///
  /*--cef()--*/
  virtual bool OnDispatchKeyEvent(
                const CefKeyEvent& event,
                CefEventHandle os_event) = 0;

  ///
  // Called on setting cursor location
  ///
  /*--cef()--*/
  virtual void OnSetCursorLocation(const CefRect& rect) = 0;

  ///
  // Called on reset
  ///
  /*--cef()--*/
  virtual void OnReset() = 0;

  ///
  // Called on focus
  ///
  /*--cef()--*/
  virtual void OnFocus() = 0;

  ///
  // Called on blur
  ///
  /*--cef()--*/
  virtual void OnBlur() = 0;
};

#endif  // CEF_INCLUDE_CEF_IME_HANDLER_H_
