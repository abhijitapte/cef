// Copyright (c) 2014 The Chromium Embedded Framework Authors.
// Portions Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "tests/cefclient/browser/ime_handler_gtk.h"

#include <X11/extensions/XInput2.h>


#include <gdk/gdk.h>
#include <gdk/gdkkeysyms.h>
#include <gdk/gdkx.h>
#include <stddef.h>

#include <gtk/gtk.h>

#include "include/base/cef_logging.h"
#include "include/base/cef_macros.h"
#include "include/wrapper/cef_helpers.h"

namespace client {

ClientImeHandlerGtk::ClientImeHandlerGtk()
  : gtk_context_(nullptr) {}

ClientImeHandlerGtk::~ClientImeHandlerGtk() {
  if (gtk_context_) {
    g_object_unref(gtk_context_);
    gtk_context_ = nullptr;
  }
}

namespace {
template <class T, class R, R (*F)(T*)>
struct XObjectDeleter {
  inline void operator()(void* ptr) const { F(static_cast<T*>(ptr)); }
};

template <class T, class D = XObjectDeleter<void, int, XFree>>
using XScopedPtr = std::unique_ptr<T, D>;

void InitXKeyEventFromXIDeviceEvent(const XEvent& src, XEvent* xkeyevent) {
  DCHECK(src.type == GenericEvent);
  XIDeviceEvent* xievent = static_cast<XIDeviceEvent*>(src.xcookie.data);
  switch (xievent->evtype) {
    case XI_KeyPress:
      xkeyevent->type = KeyPress;
      break;
    case XI_KeyRelease:
      xkeyevent->type = KeyRelease;
      break;
    default:
      NOTREACHED();
  }
  xkeyevent->xkey.serial = xievent->serial;
  xkeyevent->xkey.send_event = xievent->send_event;
  xkeyevent->xkey.display = xievent->display;
  xkeyevent->xkey.window = xievent->event;
  xkeyevent->xkey.root = xievent->root;
  xkeyevent->xkey.subwindow = xievent->child;
  xkeyevent->xkey.time = xievent->time;
  xkeyevent->xkey.x = xievent->event_x;
  xkeyevent->xkey.y = xievent->event_y;
  xkeyevent->xkey.x_root = xievent->root_x;
  xkeyevent->xkey.y_root = xievent->root_y;
  xkeyevent->xkey.state = xievent->mods.effective;
  xkeyevent->xkey.keycode = xievent->detail;
  xkeyevent->xkey.same_screen = 1;
}

}

void ClientImeHandlerGtk::ResetXModifierKeycodesCache() {
  modifier_keycodes_.clear();
  meta_keycodes_.clear();
  super_keycodes_.clear();
  hyper_keycodes_.clear();

  Display* display = cef_get_xdisplay();
  XScopedPtr<XModifierKeymap,
                  XObjectDeleter<XModifierKeymap, int, XFreeModifiermap>>
      modmap(XGetModifierMapping(display));
  int min_keycode = 0;
  int max_keycode = 0;
  int keysyms_per_keycode = 1;
  XDisplayKeycodes(display, &min_keycode, &max_keycode);
  XScopedPtr<KeySym[]> keysyms(
      XGetKeyboardMapping(display, min_keycode, max_keycode - min_keycode + 1,
                          &keysyms_per_keycode));
  for (int i = 0; i < 8 * modmap->max_keypermod; ++i) {
    const int keycode = modmap->modifiermap[i];
    if (!keycode)
      continue;
    modifier_keycodes_.insert(keycode);

    if (!keysyms)
      continue;
    for (int j = 0; j < keysyms_per_keycode; ++j) {
      switch (keysyms[(keycode - min_keycode) * keysyms_per_keycode + j]) {
        case XK_Meta_L:
        case XK_Meta_R:
          meta_keycodes_.push_back(keycode);
          break;
        case XK_Super_L:
        case XK_Super_R:
          super_keycodes_.push_back(keycode);
          break;
        case XK_Hyper_L:
        case XK_Hyper_R:
          hyper_keycodes_.push_back(keycode);
          break;
      }
    }
  }
}

GdkEvent* ClientImeHandlerGtk::GdkEventFromNativeEvent(CefEventHandle native_event) {
  if (!native_event)
    return nullptr;

  XEvent xkeyevent;
  if (native_event->type == GenericEvent) {
    // If this is an XI2 key event, build a matching core X event, to avoid
    // having two cases for every use.
    InitXKeyEventFromXIDeviceEvent(*native_event, &xkeyevent);
  } else {
    DCHECK(native_event->type == KeyPress || native_event->type == KeyRelease);
    xkeyevent.xkey = native_event->xkey;
  }
  XKeyEvent& xkey = native_event->xkey; //xkeyevent.xkey;

  // Get a GdkDisplay.
  GdkDisplay* display = gdk_x11_lookup_xdisplay(xkey.display);
  Display* x_display = xkey.display;
  if (!display) {
    // Fall back to the default display.
    display = gdk_display_get_default();
    x_display = cef_get_xdisplay();
  }
  if (!display) {
    LOG(ERROR) << "Cannot get a GdkDisplay for a key event.";
    return nullptr;
  }
  // Get a keysym and group.
  KeySym keysym = NoSymbol;
  guint8 keyboard_group = 0;
  XLookupString(&xkey, nullptr, 0, &keysym, nullptr);
  GdkKeymap* keymap = gdk_keymap_get_for_display(display);
  GdkKeymapKey* keys = nullptr;
  guint* keyvals = nullptr;
  gint n_entries = 0;
  if (keymap && gdk_keymap_get_entries_for_keycode(keymap, xkey.keycode, &keys,
                                                   &keyvals, &n_entries)) {
    for (gint i = 0; i < n_entries; ++i) {
      if (keyvals[i] == keysym) {
        keyboard_group = keys[i].group;
        break;
      }
    }
  }
  g_free(keys);
  keys = nullptr;
  g_free(keyvals);
  keyvals = nullptr;
// Get a GdkWindow.
#if GTK_CHECK_VERSION(2, 24, 0)
  GdkWindow* window = gdk_x11_window_lookup_for_display(display, xkey.window);
#else
  GdkWindow* window = gdk_window_lookup_for_display(display, xkey.window);
#endif
  if (window)
    g_object_ref(window);
  else
#if GTK_CHECK_VERSION(2, 24, 0)
    window = gdk_x11_window_foreign_new_for_display(display, xkey.window);
#else
    window = gdk_window_foreign_new_for_display(display, xkey.window);
#endif
  if (!window) {
    LOG(ERROR) << "Cannot get a GdkWindow for a key event.";
    return nullptr;
  }

  // Create a GdkEvent.
  GdkEventType event_type =
      xkey.type == KeyPress ? GDK_KEY_PRESS : GDK_KEY_RELEASE;
  GdkEvent* event = gdk_event_new(event_type);
  event->key.type = event_type;
  event->key.window = window;
  // GdkEventKey and XKeyEvent share the same definition for time and state.
  event->key.send_event = xkey.send_event;
  event->key.time = xkey.time;
  event->key.state = xkey.state;
  event->key.keyval = keysym;
  event->key.length = 0;
  event->key.string = nullptr;
  event->key.hardware_keycode = xkey.keycode;
  event->key.group = keyboard_group;
  event->key.is_modifier = IsKeycodeModifierKey(xkey.keycode);


  char keybits[32] = {0};

  XQueryKeymap(x_display, keybits);
  if (IsAnyOfKeycodesPressed(meta_keycodes_, keybits, sizeof keybits * 8))
    event->key.state |= GDK_META_MASK;
  if (IsAnyOfKeycodesPressed(super_keycodes_, keybits, sizeof keybits * 8))
    event->key.state |= GDK_SUPER_MASK;
  if (IsAnyOfKeycodesPressed(hyper_keycodes_, keybits, sizeof keybits * 8))
    event->key.state |= GDK_HYPER_MASK;

  return event;
}

bool ClientImeHandlerGtk::IsKeycodeModifierKey(
    unsigned int keycode) const {
  return modifier_keycodes_.find(keycode) != modifier_keycodes_.end();
}

bool ClientImeHandlerGtk::IsAnyOfKeycodesPressed(
    const std::vector<int>& keycodes,
    const char* keybits,
    int num_keys) const {
  for (size_t i = 0; i < keycodes.size(); ++i) {
    const int keycode = keycodes[i];
    if (keycode < 0 || num_keys <= keycode)
      continue;
    if (keybits[keycode / 8] & 1 << (keycode % 8))
      return true;
  }
  return false;
}


// GtkIMContext event handlers.
static void OnCommitThunk(GtkIMContext* context, gchar* text, ClientImeHandlerGtk* handler)
{
  handler->OnCommit(context, text);
}

static void OnPreeditChangedThunk(GtkIMContext* context, ClientImeHandlerGtk* handler)
{
  handler->OnPreeditChanged(context);
}

static void OnPreeditEndThunk(GtkIMContext* context, ClientImeHandlerGtk* handler)
{
  handler->OnPreeditEnd(context);
}

static void OnPreeditStartThunk(GtkIMContext* context, ClientImeHandlerGtk* handler)
{
  handler->OnPreeditStart(context);
}

void ClientImeHandlerGtk::OnContextCreated(bool is_simple, CefRefPtr<CefImeCallback> callback)
{
  ResetXModifierKeycodesCache();

  gtk_context_ =
        is_simple ? gtk_im_context_simple_new() : gtk_im_multicontext_new();
  ime_callback_ = callback;

  g_signal_connect(gtk_context_, "commit", G_CALLBACK(OnCommitThunk), this);
  g_signal_connect(gtk_context_, "preedit-changed",
                   G_CALLBACK(OnPreeditChangedThunk), this);
  g_signal_connect(gtk_context_, "preedit-end", G_CALLBACK(OnPreeditEndThunk),
                   this);
  g_signal_connect(gtk_context_, "preedit-start",
                   G_CALLBACK(OnPreeditStartThunk), this);
}

bool ClientImeHandlerGtk::OnDispatchKeyEvent(
                            const CefKeyEvent& cef_key_event,
                            CefEventHandle native_event)
{
  // Translate a XKeyEvent to a GdkEventKey.
  GdkEvent* event = GdkEventFromNativeEvent(native_event);
  if (!event) {
    LOG(ERROR) << "Cannot translate a XKeyEvent to a GdkEvent.";
    return false;
  }

  if (event->key.window != gdk_last_set_client_window_) {
    gtk_im_context_set_client_window(gtk_context_, event->key.window);
    gdk_last_set_client_window_ = event->key.window;
  }

  // Convert the last known caret bounds relative to the screen coordinates
  // to a GdkRectangle relative to the client window.
  gint x = 0;
  gint y = 0;
  gdk_window_get_origin(event->key.window, &x, &y);

  GdkRectangle gdk_rect = {
      last_caret_bounds_.x - x, last_caret_bounds_.y - y,
      last_caret_bounds_.width, last_caret_bounds_.height};
  gtk_im_context_set_cursor_location(gtk_context_, &gdk_rect);

  const bool handled =
      gtk_im_context_filter_keypress(gtk_context_, &event->key);
  gdk_event_free(event);

  return handled;
}

void ClientImeHandlerGtk::OnSetCursorLocation(const CefRect& rect)
{
  last_caret_bounds_ = rect;
}

void ClientImeHandlerGtk::OnReset()
{
  if (gtk_context_)
    gtk_im_context_reset(gtk_context_);
}

void ClientImeHandlerGtk::OnFocus()
{
  if (gtk_context_)
    gtk_im_context_focus_in(gtk_context_);
}

void ClientImeHandlerGtk::OnBlur()
{
  if (gtk_context_)
    gtk_im_context_focus_out(gtk_context_);
}


void ClientImeHandlerGtk::OnCommit(GtkIMContext* context,
                                             gchar* text) {
  if (context != gtk_context_)
    return;
  if (ime_callback_.get())
  {
    CefString commit_text(text);
    ime_callback_->Commit(commit_text);
  }

  CefString composition_string(text);

  // TODO: Connect this to the RealInputMethodContext
#if 0
  delegate_->OnCommit(base::UTF8ToUTF16(text));
#endif
}

void ClientImeHandlerGtk::OnPreeditChanged(GtkIMContext* context) {
  if (context != gtk_context_)
    return;

  gchar* str = nullptr;
  PangoAttrList* attrs = nullptr;
  gint cursor_pos = 0;
  gtk_im_context_get_preedit_string(context, &str, &attrs, &cursor_pos);

  CefString composition_string(str);

  // TODO: Connect this to the RealInputMethodContext
  // Should use CefCompositionUnderline, CefRange?!?
  // Usage in ImeSetComposition
#if 0
  // From x11_input_method_context_impl_gtk
  ui::CompositionText composition_text;
  ui::ExtractCompositionTextFromGtkPreedit(str, attrs, cursor_pos,
                                           &composition_text);
#endif
  g_free(str);
  pango_attr_list_unref(attrs);

#if 0
  delegate_->OnPreeditChanged(composition_text);
#endif
}

void ClientImeHandlerGtk::OnPreeditEnd(GtkIMContext* context) {
  if (context != gtk_context_)
    return;

  if (ime_callback_.get())
  {
    ime_callback_->PreeditEnd();
  }

  // TODO: Connect this to the RealInputMethodContext

#if 0
  delegate_->OnPreeditEnd();
#endif
}

void ClientImeHandlerGtk::OnPreeditStart(GtkIMContext* context) {
  if (context != gtk_context_)
    return;

  if (ime_callback_.get())
  {
    ime_callback_->PreeditStart();
  }

  // TODO: Connect this to the RealInputMethodContext

#if 0
  delegate_->OnPreeditStart();
#endif
}

}  // namespace client
