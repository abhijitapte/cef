// Copyright (c) 2014 The Chromium Embedded Framework Authors.
// Portions Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef CEF_TESTS_CEFCLIENT_BROWSER_IME_HANDLER_GTK_H_
#define CEF_TESTS_CEFCLIENT_BROWSER_IME_HANDLER_GTK_H_
#pragma once

#include "include/cef_ime_handler.h"

#include <vector>
#include <unordered_set>

typedef union _GdkEvent GdkEvent;
typedef struct _GtkIMContext GtkIMContext;
typedef char gchar;
typedef void* gpointer;

namespace client {

class ClientImeHandlerGtk : public CefImeHandler {
 public:
  ClientImeHandlerGtk();
  virtual ~ClientImeHandlerGtk();

  void OnContextCreated(bool is_simple, CefRefPtr<CefImeCallback> callback) OVERRIDE;

  // ClientImeHandler methods.
  bool OnDispatchKeyEvent(
                const CefKeyEvent& event,
                CefEventHandle os_event) OVERRIDE;

  void OnSetCursorLocation(const CefRect& rect) OVERRIDE;

  void OnReset() OVERRIDE;

  void OnFocus() OVERRIDE;

  void OnBlur() OVERRIDE;

  // GtkIMContext methods
  void OnCommit(GtkIMContext* context, gchar* text);
  void OnPreeditChanged(GtkIMContext* context);
  void OnPreeditEnd(GtkIMContext* context);
  void OnPreeditStart(GtkIMContext* context);

 private:

  void ResetXModifierKeycodesCache();
  GdkEvent* GdkEventFromNativeEvent(CefEventHandle native_event);
  bool IsKeycodeModifierKey(unsigned int keycode) const;
  bool IsAnyOfKeycodesPressed(
        const std::vector<int>& keycodes,
        const char* keybits,
        int num_keys) const;

  GtkIMContext* gtk_context_;
  gpointer gdk_last_set_client_window_;

  std::unordered_set<unsigned int> modifier_keycodes_;

  // A list of keycodes of each modifier key.
  std::vector<int> meta_keycodes_;
  std::vector<int> super_keycodes_;
  std::vector<int> hyper_keycodes_;

  CefRect last_caret_bounds_;
  CefRefPtr<CefImeCallback> ime_callback_;

  IMPLEMENT_REFCOUNTING(ClientImeHandlerGtk);
  DISALLOW_COPY_AND_ASSIGN(ClientImeHandlerGtk);
};

}  // namespace client

#endif  // CEF_TESTS_CEFCLIENT_BROWSER_IME_HANDLER_GTK_H_
