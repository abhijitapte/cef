
#ifndef CEF_IME_LINUX_FAKE_INPUT_METHOD_CONTEXT_H_
#define CEF_IME_LINUX_FAKE_INPUT_METHOD_CONTEXT_H_

#include "base/macros.h"
#include "ui/base/ime/linux/linux_input_method_context.h"
#include "include/cef_ime_handler.h"
#include "libcef/browser/thread_util.h"
#include "libcef/common/content_client.h"


namespace ime {

// A real implementation of LinuxInputMethodContext, which does everything.
class RealInputMethodContext : public ui::LinuxInputMethodContext/*, public base::RefCountedThreadSafe<
                                RealInputMethodContext,
                                content::BrowserThread::DeleteOnUIThread> */ {
 public:
  RealInputMethodContext();
  RealInputMethodContext(ui::LinuxInputMethodContextDelegate* delegate, bool is_simple = false);

  // Overriden from ui::LinuxInputMethodContext
  bool DispatchKeyEvent(const ui::KeyEvent& key_event) override;
  void Reset() override;
  void Focus() override;
  void Blur() override;
  void SetCursorLocation(const gfx::Rect& rect) override;


  void Commit(const CefString& text);
  void PreeditChanged(const CefString& text);
  void PreeditEnd();
  void PreeditStart();

 private:

  void OnContextCreated();

  DISALLOW_COPY_AND_ASSIGN(RealInputMethodContext);

  ui::LinuxInputMethodContextDelegate* delegate_;
  CefRefPtr<CefImeHandler> handler_;
  bool is_simple_;
};

}  // namespace ime

#endif  // CEF_IME_LINUX_FAKE_INPUT_METHOD_CONTEXT_H_
