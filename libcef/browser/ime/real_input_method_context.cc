
#include "real_input_method_context.h"
#include "base/logging.h"
#include "libcef/common/content_client.h"
#include "ui/events/event.h"
#include "libcef/browser/browser_util.h"
#include "libcef/browser/thread_util.h"

#include "base/bind.h"


namespace ime {

class CefImeCallbackImpl : public CefImeCallback {
 public:
  explicit CefImeCallbackImpl(RealInputMethodContext* imContext)
      : im_context_(imContext) {}

  void Commit(const CefString& text) override {
    if (CEF_CURRENTLY_ON_UIT()) {
      if (im_context_) {
        im_context_->Commit(text);
      }
    } else {
      CEF_POST_TASK(CEF_UIT, base::Bind(&CefImeCallbackImpl::Commit,
                                        this, text));
    }
  }

  void PreeditChanged(const CefString& text) override {
    if (CEF_CURRENTLY_ON_UIT()) {
      if (im_context_) {
        im_context_->PreeditChanged(text);
      }
    } else {
      CEF_POST_TASK(CEF_UIT, base::Bind(&CefImeCallbackImpl::PreeditChanged,
                                        this, text));
    }
  }

  void PreeditEnd() override {
    if (CEF_CURRENTLY_ON_UIT()) {
      if (im_context_) {
        im_context_->PreeditEnd();
      }
    } else {
      CEF_POST_TASK(CEF_UIT, base::Bind(&CefImeCallbackImpl::PreeditEnd,
                                        this));
    }
  }

  void PreeditStart() override {
    if (CEF_CURRENTLY_ON_UIT()) {
      if (im_context_) {
        im_context_->PreeditStart();
      }
    } else {
      CEF_POST_TASK(CEF_UIT, base::Bind(&CefImeCallbackImpl::PreeditStart,
                                        this));
    }
  }

 private:
  RealInputMethodContext* im_context_;

  IMPLEMENT_REFCOUNTING(CefImeCallbackImpl);
  DISALLOW_COPY_AND_ASSIGN(CefImeCallbackImpl);
};

RealInputMethodContext::RealInputMethodContext()
    : delegate_(nullptr)
    , handler_(nullptr)
    , is_simple_(false)
{

}

RealInputMethodContext::RealInputMethodContext(
    ui::LinuxInputMethodContextDelegate* delegate,
    bool is_simple)
    : delegate_(delegate)
    , handler_(nullptr)
    , is_simple_(is_simple)
{
  CHECK(delegate_);
  CefRefPtr<CefApp> app = CefContentClient::Get()->application();
  if (app.get()) {
    CefRefPtr<CefBrowserProcessHandler> browser_handler =
        app->GetBrowserProcessHandler();
    if (browser_handler.get())
      handler_ = browser_handler->GetImeHandler();
  }

  if (handler_)
  {
    OnContextCreated();
    //CEF_POST_TASK(CEF_UIT, base::Bind(&RealInputMethodContext::OnContextCreated,
    //                                    this));
  }
}

void RealInputMethodContext::OnContextCreated()
{
  if (CEF_CURRENTLY_ON_UIT() && handler_)
  {
    CefRefPtr<CefImeCallbackImpl> callback_impl = new CefImeCallbackImpl(this);
    handler_->OnContextCreated(is_simple_, callback_impl);
  }
}

// Overriden from ui::LinuxInputMethodContext

bool RealInputMethodContext::DispatchKeyEvent(
    const ui::KeyEvent& key_event) {
  if (!key_event.HasNativeEvent() || !handler_)
    return false;

  // Convert the key_event to cef_key_event and os_event
  CefKeyEvent cef_key_event;
  CefEventHandle os_event;

  if (!browser_util::GetCefKeyEvent(key_event, cef_key_event))
    return false;

  os_event = key_event.native_event();

  return handler_->OnDispatchKeyEvent(cef_key_event, os_event);
}

void RealInputMethodContext::Reset() {
  if (handler_)
    handler_->OnReset();
}

void RealInputMethodContext::Focus() {
  if (handler_)
    handler_->OnFocus();
}

void RealInputMethodContext::Blur() {
  if (handler_)
    handler_->OnBlur();
}

void RealInputMethodContext::SetCursorLocation(const gfx::Rect& rect) {
  if (handler_) {
      CefRect cef_rect(rect.x(), rect.y(),
                   rect.width(), rect.height());
      handler_->OnSetCursorLocation(cef_rect);
  }
}

void RealInputMethodContext::Commit(const CefString& text)
{
  delegate_->OnCommit(text);
}

void RealInputMethodContext::PreeditChanged(const CefString& text)
{

}

void RealInputMethodContext::PreeditEnd()
{
  delegate_->OnPreeditEnd();
}

void RealInputMethodContext::PreeditStart()
{
  delegate_->OnPreeditStart();
}

}  // namespace ime
