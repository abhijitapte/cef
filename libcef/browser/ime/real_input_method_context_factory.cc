
#include "real_input_method_context_factory.h"

#include "base/memory/ptr_util.h"
#include "real_input_method_context.h"

namespace ime {

RealInputMethodContextFactory::RealInputMethodContextFactory() {}

std::unique_ptr<ui::LinuxInputMethodContext>
RealInputMethodContextFactory::CreateInputMethodContext(
    ui::LinuxInputMethodContextDelegate* delegate,
    bool /*is_simple */) const {
  return std::make_unique<RealInputMethodContext>(delegate);
}

void RealInputMethodContextFactory::SetInstance(RealInputMethodContextFactory* instance) {
  ui::LinuxInputMethodContextFactory::SetInstance(instance);
}

//static
RealInputMethodContextFactory* RealInputMethodContextFactory::CreateInputMethodContextFactory()
{
  return new RealInputMethodContextFactory();
}

}  // namespace ime
