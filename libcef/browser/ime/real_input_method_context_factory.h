
#ifndef CEF_IME_LINUX_REAL_INPUT_METHOD_CONTEXT_FACTORY_H_
#define CEF_IME_LINUX_REAL_INPUT_METHOD_CONTEXT_FACTORY_H_

#include "base/macros.h"
#include "ui/base/ime/linux/linux_input_method_context_factory.h"

namespace ime {

// An implementation of LinuxInputMethodContextFactory, which creates and
// returns RealInputMethodContext's.
class RealInputMethodContextFactory : public ui::LinuxInputMethodContextFactory {
 public:
  RealInputMethodContextFactory();

  static RealInputMethodContextFactory* CreateInputMethodContextFactory();

  // LinuxInputMethodContextFactory:
  std::unique_ptr<ui::LinuxInputMethodContext> CreateInputMethodContext(
      ui::LinuxInputMethodContextDelegate* delegate,
      bool is_simple) const override;

  static void SetInstance(RealInputMethodContextFactory* instance);

 private:
  DISALLOW_COPY_AND_ASSIGN(RealInputMethodContextFactory);
};

}  // namespace ime

#endif  // CEF_IME_LINUX_REAL_INPUT_METHOD_CONTEXT_FACTORY_H_
